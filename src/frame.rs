pub trait Frame {}

#[macro_export]
macro_rules! new_frame {
    ($name:ident) => {
        #[derive(Debug, PartialEq, Eq)]
        struct $name {}

        impl Frame for $name {}
    }
}
