extern crate nalgebra as na;

use types::Float;
use frame::Frame;

use std::marker::PhantomData;

use na::Matrix4;

/**
  A homogeneous matrix representing a transformation from frame `F` to frame `T`
*/
#[derive(PartialEq, Debug)]
pub struct HMatrix<F: Frame, T: Frame> {
    value: Matrix4<Float>,
    _phantom: (PhantomData<F>, PhantomData<T>)
}

impl<F: Frame, T: Frame> HMatrix<F, T> {
    pub fn new(value: Matrix4<Float>) -> Self {
        Self {
            value,
            _phantom: (PhantomData, PhantomData),
        }
    }

    /**
      Post-multiplies `self` with the specified right hand side. The result
      is a transformation from `F` to `To`
    */
    pub fn chain<To: Frame>(&self, right: HMatrix<T, To>) -> HMatrix<F, To> {
        HMatrix::new(self.value * right.value)
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn chaining_works() {
        new_frame!(Frame1);
        new_frame!(Frame2);
        new_frame!(Frame3);

        let left = HMatrix::<Frame1, Frame2>::new(Matrix4::new(
                0., -1., 0., 0.,
                1., 0.,  0., 0.,
                0., 0.,  1., 0.,
                0., 0.,  0., 1.
            ));
        let right = HMatrix::<Frame2, Frame3>::new(Matrix4::new(
                1., 0., 0., 1.,
                0., 1., 0., 0.,
                0., 0., 1., 0.,
                0., 0., 0., 1.,
            ));
        let chained = left.chain(right);

        let expected = HMatrix::<Frame1, Frame3>::new(Matrix4::new(
                    0., -1., 0., 0.,
                    1., 0.,  0., 1.,
                    0., 0.,  1., 0.,
                    0., 0.,  0., 1.,
                ));

        assert_eq!(chained, expected);
    }
}

