extern crate nalgebra as na;

use std::marker::PhantomData;

use na::{Vector6, Matrix4, Vector3};

use frame::Frame;
use types::Float;
use util::{untilde, tilde_form};

#[derive(Debug, PartialEq)]
pub struct Twist<Fr: Frame, To: Frame, In: Frame> {
    tilde_form: Matrix4<Float>,
    vector_form: Vector6<Float>,
    _phantom: (PhantomData<Fr>, PhantomData<To>, PhantomData<In>)
}

impl<Fr: Frame, To: Frame, In: Frame> Twist<Fr, To, In> {
    fn new(vector_form: Vector6<Float>, tilde_form: Matrix4<Float>) -> Self {
        Self {
            tilde_form,
            vector_form,
            _phantom: (PhantomData, PhantomData, PhantomData)
        }
    }

    pub fn from_vector(vector_form: Vector6<Float>) -> Self {
        let tilde_form = Self::vector_to_tilde(&vector_form);
        Self::new(vector_form, tilde_form)
    }

    pub fn from_tilde(tilde_form: Matrix4<Float>) -> Self {
        let vector_form = Self::tilde_to_vector(&tilde_form);
        Self::new(vector_form, tilde_form)
    }

    fn vector_to_tilde(vector_form: &Vector6<Float>) -> Matrix4<Float> {
        let rotation = vector_form.fixed_rows::<na::U3>(0).into_owned();
        let velocity = vector_form.fixed_rows::<na::U3>(3).into_owned();

        let rotation_tilde = tilde_form(rotation);

        let mut expanded = rotation_tilde.insert_row(3, 0.).insert_column(3, 0.);
        expanded[(0,3)] = velocity[0];
        expanded[(1,3)] = velocity[1];
        expanded[(2,3)] = velocity[2];
        expanded
    }

    fn tilde_to_vector(matrix_form: &Matrix4<Float>) -> Vector6<Float> {
        let rotation = matrix_form.fixed_slice::<na::U3, na::U3>(0, 0).into_owned();

        let rotation_vector = untilde(rotation);

        let mut result = rotation_vector.insert_fixed_rows::<na::U3>(3, 0.);
        result[3] = matrix_form[(0,3)];
        result[4] = matrix_form[(1,3)];
        result[5] = matrix_form[(2,3)];
        result
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    new_frame!(F1);
    new_frame!(F2);

    #[test]
    fn vector_to_tilde_is_correct() {
        let vector = Vector6::new(1.,2.,3., 4.,5.,6.);
        let matrix = Matrix4::new(
                0., -3., 2., 4.,
                3., 0., -1., 5.,
                -2., 1., 0., 6.,
                0., 0., 0., 0.,
            );

        assert_eq!(Twist::<F1, F2, F2>::vector_to_tilde(&vector), matrix);
    }

    #[test]
    fn tilde_to_vector_is_correct() {
        let vector = Vector6::new(1.,2.,3., 4.,5.,6.);
        let matrix = Matrix4::new(
                0., -3., 2., 4.,
                3., 0., -1., 5.,
                -2., 1., 0., 6.,
                0., 0., 0., 0.,
            );

        assert_eq!(Twist::<F1, F2, F2>::tilde_to_vector(&matrix), vector);
    }
}
