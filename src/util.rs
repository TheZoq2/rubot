extern crate nalgebra as na;

use types::Float;

use na::{Vector3, Matrix3};

// TODO: Maybe make generic over all float types
pub fn tilde_form(vector: Vector3<Float>) -> Matrix3<Float> {
    Matrix3::new(
            0.,          -vector[2], vector[1],
            vector[2],  0.,          -vector[0],
            -vector[1], vector[0], 0.
        )
}

pub fn untilde(matrix: Matrix3<Float>) -> Vector3<Float> {
    Vector3::new(matrix[(2,1)], -matrix[(2,0)], matrix[(1, 0)])
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn tilde_form_is_correct() {
        let vector = Vector3::new(3.,2.,1.);
        let matrix = Matrix3::new(
                0.,  -1., 2.,
                1.,  0.,  -3.,
                -2., 3.,  0.
            );

        assert_eq!(tilde_form(vector), matrix);
    }

    #[test]
    fn untilde_is_correct() {
        let matrix = Matrix3::new(
                0.,  -1., 2.,
                1.,  0.,  -3.,
                -2., 3.,  0.
            );
        let vector = Vector3::new(3.,2.,1.);

        assert_eq!(untilde(matrix), vector);
    }
}
